import numpy as np
import torch
from .utils.utils import wrap_angles, MovingObstacle2DPolygon, RectangleRegionArray, StampedPointArraySE2, RoadMapPolygon

class TrajectoryInitializer(object):
    def __init__(self, collision_checker):
        self._collision_checker = collision_checker

    def initialize_trajectory(self, trajectory: torch.Tensor, start_point: np.ndarray, goal_point: np.ndarray, length: int):
        with torch.no_grad():
            trajectory[:, 0] = torch.linspace(start_point[0], goal_point[0], length)
            trajectory[:, 1] = torch.linspace(start_point[1], goal_point[1], length)
            # trajectory[:, 2] = torch.linspace(start_point[2], goal_point[2], length)
            self.initialize_angle(trajectory, start_point, goal_point, length)

    def initialize_angle(self, trajectory: torch.Tensor, start_point: np.ndarray, goal_point: np.ndarray, length: int):
        with torch.no_grad():
            delta_angle = wrap_angles(goal_point[2] - start_point[2])
            goal_angle = delta_angle + start_point[2]
            trajectory[:, 2] = torch.linspace(start_point[2], goal_angle, length)