import numpy as np
import torch
import scipy

from .jps import JPS
from ..trajectory_initializer import TrajectoryInitializer
from ..utils.utils import RectangleRegionArray, StampedPointArraySE2, PointArraySE2

class AstarTrajectoryInitializer(TrajectoryInitializer):
    def __init__(self, collision_checker, resolution):
        super().__init__(collision_checker)
        self._resolution = resolution

    def reparametrize_path(self, path: np.ndarray, point_count: np.ndarray):
        distances = np.linalg.norm(path[1:] - path[:-1], axis=1) + 1e-6
        cum_distances = np.cumsum(distances)
        cum_distances = np.concatenate([np.zeros(1), cum_distances], axis=0)
        parametrization = cum_distances / cum_distances[-1]
        new_parametrization = np.linspace(0, 1, point_count)
        interpolated_trajectory = scipy.interpolate.interp1d(parametrization, path, kind="quadratic", axis=0,
                                                            fill_value="extrapolate")
        return interpolated_trajectory(new_parametrization)

    def initialize_trajectory(self, trajectory: torch.Tensor, start_point: np.ndarray, goal_point: np.ndarray, length: int):
        path = self.calculate_astar_path(start_point, goal_point)
        path = np.concatenate([start_point[None, :2], path, goal_point[None, :2]], axis=0)
        reparametrized_path = self.reparametrize_path(path=path, point_count=length)
        with torch.no_grad():
            trajectory[:, :2] = torch.tensor(reparametrized_path.astype(np.float32), device=trajectory.device)
        self.initialize_angle(trajectory, start_point, goal_point, length)

    def wrap_angles(self,angles):
        return (angles + np.pi) % (2 * np.pi) - np.pi

    def calculate_astar_path(self, start: np.ndarray, goal: np.ndarray) -> np.ndarray:
        boundaries:RectangleRegionArray = self._collision_checker.get_boundaries()
        x_cells = int((boundaries.max_x - boundaries.min_x) // self._resolution) + 1
        y_cells = int((boundaries.max_y - boundaries.min_y) // self._resolution) + 1
        start_x_cell = int((start[0] - boundaries.min_x) // self._resolution)
        start_y_cell = int((start[1] - boundaries.min_y) // self._resolution)
        goal_x_cell = int((goal[0] - boundaries.min_x) // self._resolution)
        goal_y_cell = int((goal[1] - boundaries.min_y) // self._resolution)
        x, y = np.meshgrid(range(x_cells), range(y_cells))
        x = x.reshape(-1) * self._resolution + self._resolution / 2 + boundaries.min_x
        y = y.reshape(-1) * self._resolution + self._resolution / 2 + boundaries.min_y
        t = start[2] + np.linspace(0, self.wrap_angles(goal[2] - start[2]), len(x))
        positions = StampedPointArraySE2(np.linspace(0, 0, len(x)), PointArraySE2(x, y, t))
        collisions: np.ndarray = self._collision_checker.is_collision(positions)
        matrix = collisions.reshape(y_cells, x_cells)
        matrix[goal_y_cell, goal_x_cell] = False
        planner = JPS(matrix, jps=False)
        path = planner.find_path((start_y_cell, start_x_cell), (goal_y_cell, goal_x_cell))
        final_path = np.zeros_like(path).astype(np.float32)
        final_path[:, 0] = path[:, 1] * self._resolution + self._resolution / 2 + boundaries.min_x
        final_path[:, 1] = path[:, 0] * self._resolution + self._resolution / 2 + boundaries.min_y
        return final_path
