
import dataclasses
from dataclasses import dataclass

import numpy as np
# import plotly.graph_objects as go
import torch
import torch.nn as nn
import torch.nn.functional
from typing import List, Optional

from dynamic_nfomp.utils.utils import MovingObstacle2DPolygon, RectangleRegionArray, StampedPointArraySE2, RoadMapPolygon

class MovingPolygonCollisionDetector:
    def __init__(self, moving_obstacle_array: MovingObstacle2DPolygon,
                 outside_rectangle_region_array: RectangleRegionArray,
                 road_map: RoadMapPolygon,
                 length: float, width: float, collision_method: str):
        self.moving_obstacle_array = moving_obstacle_array
        self.outside_rectangle_region_array = outside_rectangle_region_array
        self.road_map = road_map
        self.ego_length = length
        self.ego_width = width
        self.collision_method = collision_method

    def is_collision(self, points: StampedPointArraySE2) -> np.ndarray((), dtype=bool):
        res =  (~self.outside_rectangle_region_array.inside(points.points)) | \
                self.moving_obstacle_array.inside(points, self.ego_length, self.ego_width, self.collision_method) | \
                ~self.road_map.inside(points.points, self.ego_length, self.ego_width, self.collision_method)
        # res =  (~self.outside_rectangle_region_array.inside(points.points)) | \
        #         self.moving_obstacle_array.inside(points, self.ego_length, self.ego_width, self.collision_method)
        return res

    def get_boundaries(self):
        return self.outside_rectangle_region_array

    @classmethod
    def from_dict(cls, data):
        moving_obstacle_array = MovingObstacle2DPolygon.from_dict(data['moving_obstacle_array'])
        outside_rectangle_region_array = RectangleRegionArray.from_dict(data['outside_rectangle_region_array'])
        road_map = RoadMapPolygon.from_dict(data['road_map_path'], outside_rectangle_region_array)
        length = data['ego_length']
        width = data['ego_width']
        collision_method = data['collision_method']
        
        return cls(moving_obstacle_array, outside_rectangle_region_array, road_map, length, width, collision_method)
    
