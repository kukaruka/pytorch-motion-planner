
import dataclasses
from dataclasses import dataclass

import numpy as np
# import plotly.graph_objects as go
import torch
import torch.nn as nn
import torch.nn.functional
from typing import List, Optional

from dynamic_nfomp.utils.utils import PointArray2D, RectangleRegionArray, interpolate_1d_pytorch, MovingObstacleArray1D, StampedPointArray2D
class HighwayCollisionDetector:
    def __init__(self, moving_obstacle_array: MovingObstacleArray1D,
                 outside_rectangle_region_array: RectangleRegionArray):
        self.moving_obstacle_array = moving_obstacle_array
        self.outside_rectangle_region_array = outside_rectangle_region_array

    def is_collision(self, points: StampedPointArray2D):
        return self.moving_obstacle_array.inside(points) | (
            ~self.outside_rectangle_region_array.inside(points.points))

    @classmethod
    def from_dict(cls, data):
        moving_obstacle_array = MovingObstacleArray1D.from_dict(data['moving_obstacle_array'])
        outside_rectangle_region_array = RectangleRegionArray.from_dict(data['outside_rectangle_region_array'])
        return cls(moving_obstacle_array, outside_rectangle_region_array)