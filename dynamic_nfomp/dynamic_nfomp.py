import dataclasses
from dataclasses import dataclass

import numpy as np
# import plotly.graph_objects as go
import torch
import torch.nn as nn
import torch.nn.functional
from typing import List, Optional

from dynamic_nfomp.utils.utils import PointArraySE2, interpolate_1d_pytorch
from dynamic_nfomp.utils.timer import Timer
from dynamic_nfomp.utils.universal_factory import UniversalFactory
from dynamic_nfomp.utils.utils import *
from dynamic_nfomp.collision_detector import MovingPolygonCollisionDetector
from dynamic_nfomp.trajectory_initializer import TrajectoryInitializer
from dynamic_nfomp.astar.astar_trajectory_initializer import AstarTrajectoryInitializer
from copy import copy
@dataclasses.dataclass
class ONFModelConfig:
    mean: float
    sigma: float
    input_dimension: int
    encoding_dimension: int
    hidden_dimensions: List[int]


class ONF(nn.Module):
    def __init__(self, parameters: ONFModelConfig):
        super().__init__()
        self.encoding_layer = nn.Linear(parameters.input_dimension, parameters.encoding_dimension)
        self.mlp1 = self.make_mlp(parameters.encoding_dimension, parameters.hidden_dimensions[:-1],
                                  parameters.hidden_dimensions[-1])
        self.mlp2 = self.make_mlp(parameters.encoding_dimension + parameters.hidden_dimensions[-1], [], 1)
        self._mean = parameters.mean
        self._sigma = parameters.sigma

    @classmethod
    def make_mlp(cls, input_dimension, hidden_dimensions, output_dimension):
        modules = []
        for dimension in hidden_dimensions:
            modules.append(nn.Linear(input_dimension, dimension))
            modules.append(nn.ReLU())
            input_dimension = dimension
        modules.append(nn.Linear(input_dimension, output_dimension))
        return nn.Sequential(*modules)

    def forward(self, x):
        x = (x - self._mean) / self._sigma
        x = self.encoding_layer(x)
        x = torch.sin(x)
        input_x = x
        x = self.mlp1(input_x)
        x = torch.cat([x, input_x], dim=1)
        x = self.mlp2(x)
        return x

@dataclasses.dataclass
class PathPlannerTask:
    start: PointSE2
    goal: PointSE2
    start_time: float
    maximal_velocity: float
    collision_detector: MovingPolygonCollisionDetector

@dataclasses.dataclass
class PathOptimizedState:
    positions_: torch.Tensor
    times_: torch.Tensor
    velocity_constraint_multipliers: torch.Tensor
    direction_constraint_multipliers: torch.Tensor
    start_position: torch.Tensor
    goal_position: torch.Tensor
    start_time: float

    @property
    def result_path(self) -> ResultPathSE2:
        return ResultPathSE2(
            StampedPointArraySE2(times=self.times.cpu().detach().numpy(),
                              points=PointArraySE2.from_vec(self.positions.cpu().detach().numpy()))
        )

    @property
    def positions(self) -> torch.Tensor:
        return torch.cat(
            [self.start_position[None], self.positions_, self.goal_position[None]], dim=0)

    @property
    def times(self) -> torch.Tensor:
        return torch.cat(
            [torch.tensor([self.start_time]), self.times_], dim=0)

    def reparametrize(self):
        old_times: torch.Tensor = self.times
        new_times = torch.linspace(old_times[0].item(), old_times[-1].item(), old_times.shape[0])
        self.positions_.data = interpolate_1d_pytorch(self.positions, old_times, new_times)[1:-1]
        self.times_.data = new_times[1:]
        multipliers_old_times = (old_times[:-1] + old_times[1:]) / 2
        multipliers_new_times = (new_times[:-1] + new_times[1:]) / 2
        self.direction_constraint_multipliers.data = interpolate_1d_pytorch(
            self.direction_constraint_multipliers, multipliers_old_times, multipliers_new_times)

    @property
    def stamped_positions(self) -> torch.Tensor:
        return torch.cat([self.times[:, None], self.positions], dim=1)

    @property
    def optimized_parameters(self):
        return [self.positions_, self.times_]

@dataclasses.dataclass
class OptimizerImplConfig:
    lr: float
    beta1: float
    beta2: float


class OptimizerImpl:
    def __init__(self, parameters: OptimizerImplConfig):
        self._optimizer = None
        self._parameters = parameters

    def setup(self, model_parameters):
        self._optimizer = torch.optim.Adam(model_parameters, lr=self._parameters.lr,
                                           betas=(self._parameters.beta1, self._parameters.beta2))

    def zero_grad(self):
        self._optimizer.zero_grad()

    def step(self):
        self._optimizer.step()


# @dataclasses.dataclass
# class OptimizerWithLagrangeMultipliersConfig(OptimizerImplConfig):
#     lagrange_multiplier_lr: float

@dataclasses.dataclass
class OptimizerWithLagrangeMultipliersConfig(OptimizerImplConfig):
    lagrange_multiplier_lr: float
    base_lr: float
    max_lr: float
    step_size_up: int
    step_size_down: int

class OptimizerWithLagrangeMultipliers(OptimizerImpl):
    def __init__(self, parameters: OptimizerWithLagrangeMultipliersConfig):
        super().__init__(parameters)
        self._parameters = parameters
        self._lagrange_multiplier_parameters = None
        self._scheduler = None

    # noinspection PyMethodOverriding
    def setup(self, model_parameters, lagrange_multiplier_parameters):
        self._optimizer = torch.optim.Adam(model_parameters, lr=self._parameters.lr,
                                           betas=(self._parameters.beta1, self._parameters.beta2))
        self._scheduler = torch.optim.lr_scheduler.CyclicLR(self._optimizer, base_lr=self._parameters.base_lr,
                                                            max_lr=self._parameters.max_lr,
                                                            step_size_up=self._parameters.step_size_up,
                                                            step_size_down=self._parameters.step_size_down,
                                                            cycle_momentum=False)
        self._lagrange_multiplier_parameters = lagrange_multiplier_parameters

    def zero_grad(self):
        self._optimizer.zero_grad()
        for p in self._lagrange_multiplier_parameters:
            p.grad = None

    def step(self):
        self._optimizer.step()
        with torch.no_grad():
            for p in self._lagrange_multiplier_parameters:
                p.data += self._parameters.lagrange_multiplier_lr * p.grad
        self._scheduler.step()
class PathOptimizedStateInitializer:
    def __init__(self, planner_task: PathPlannerTask, path_state_count: int, device: str,
                trajectory_initializer: TrajectoryInitializer):
        self._planner_task = planner_task
        self._path_state_count = path_state_count
        self._device = device
        self._initializer = trajectory_initializer

    def init(self)-> PathOptimizedState:
        with torch.no_grad():
            positions = self._initialize_positions()
            return PathOptimizedState(
                positions_=positions[1:-1].clone().detach().requires_grad_(True),
                times_=self._initialize_times(positions),
                velocity_constraint_multipliers=torch.zeros(self._path_state_count + 1, requires_grad=True,
                                                            device=self._device),
                start_position=torch.tensor(self._planner_task.start.as_numpy(), requires_grad=False, device=self._device, dtype=torch.float32),
                goal_position=torch.tensor(self._planner_task.goal.as_numpy(), requires_grad=False, device=self._device, dtype=torch.float32),
                start_time=self._planner_task.start_time,
                direction_constraint_multipliers=torch.zeros(self._path_state_count + 1, requires_grad=True,
                                                             device=self._device, dtype=torch.float32)
            )

    def _initialize_positions(self) -> torch.Tensor:
        trajectory_length = self._path_state_count + 2
        trajectory = torch.zeros(self._path_state_count + 2, 3, requires_grad=True, device=self._device, dtype=torch.float32)
        start_point: np.ndarray = self._planner_task.start.as_numpy()
        goal_point: np.ndarray = self._planner_task.goal.as_numpy()
        self._initializer.initialize_trajectory(trajectory, start_point, goal_point, trajectory_length)
        return trajectory

    def _initialize_times(self, positions) -> torch.Tensor:
        distances = torch.linalg.norm(positions[1:] - positions[:-1], dim=1)
        cumulative_distances = torch.cumsum(distances, dim=0)
        cumulative_distances = torch.cat([torch.zeros(1), cumulative_distances], dim=0)
        times = self._planner_task.start_time + cumulative_distances / self._planner_task.maximal_velocity
        times = times[1:].clone().detach().requires_grad_(True)
        return times


@dataclasses.dataclass
class PathLossBuilderConfig:
    distance_weight: float
    collision_weight: float
    time_regularization_weight: float
    soft_maximal_velocity_constraint_weight: float
    direction_constraint_weight: float
    second_differences_weight: float
class PathLossBuilder:
    def __init__(self, planner_task: PathPlannerTask, parameters: PathLossBuilderConfig):
        self._parameters = parameters
        self._planner_task = planner_task

    def get_loss(self, collision_model: nn.Module, optimized_state: PathOptimizedState):
        loss = self._parameters.distance_weight * self._distance_loss(optimized_state)
        loss = loss + self._parameters.collision_weight * self._collision_loss(collision_model, optimized_state)
        loss = loss + self._parameters.time_regularization_weight * self._time_regularization_loss(optimized_state)
        loss = loss + self._parameters.soft_maximal_velocity_constraint_weight * self._soft_maximal_velocity_constraint_loss(
            optimized_state)
        loss = loss + self._direction_constraint_loss(optimized_state)
        loss = loss + self._parameters.second_differences_weight * self._second_differences_loss(optimized_state)
        return loss

    def _direction_constraint_loss(self, optimized_state: PathOptimizedState):
        deltas = self.non_holonomic_constraint_deltas(optimized_state.positions)
        return self._parameters.direction_constraint_weight * torch.mean(deltas ** 2) + torch.mean(optimized_state.direction_constraint_multipliers * deltas)

    @staticmethod
    def non_holonomic_constraint_deltas(positions):
        dx = (positions[1:, 0] - positions[:-1, 0])
        dy = (positions[1:, 1] - positions[:-1, 1])
        angles = positions[:, 2]
        delta_angles = wrap_angles(angles[1:] - angles[:-1])
        mean_angles = angles[:-1] + delta_angles / 2
        return dx * torch.sin(mean_angles) - dy * torch.cos(mean_angles)

    def _second_differences_loss(self, optimized_state: PathOptimizedState):
        return torch.mean((optimized_state.positions[2:] - 2 * optimized_state.positions[1:-1] + optimized_state.positions[:-2]) ** 2)

    @staticmethod
    def _distance_loss(optimized_state: PathOptimizedState):
        path = optimized_state.positions
        return torch.mean((path[1:] - path[:-1]) ** 2)

    def _collision_loss(self, collision_model: nn.Module, optimized_state: PathOptimizedState):
        positions: torch.Tensor = optimized_state.stamped_positions
        positions = self.get_intermediate_points(positions)
        positions = positions.reshape(-1, 4)
        return torch.mean(torch.nn.functional.softplus(collision_model(positions)))

    @staticmethod
    def get_intermediate_points(positions):
        t = torch.rand(positions.shape[0] - 1, 1)
        delta = positions[1:] - positions[:-1]
        return positions[1:] + t * delta

    @staticmethod
    def _time_regularization_loss(optimized_state: PathOptimizedState):
        times = optimized_state.times
        return torch.mean((times[1:] - times[:-1]) ** 2)

    def _soft_maximal_velocity_constraint_loss(self, optimized_state: PathOptimizedState):
        positions = optimized_state.positions
        times = optimized_state.times
        distances = torch.linalg.norm(positions[1:] - positions[:-1], dim=1)
        time_differences = times[1:] - times[:-1]
        delta = time_differences * self._planner_task.maximal_velocity - distances
        return torch.mean(delta ** 2 + optimized_state.velocity_constraint_multipliers * delta)


class GradPreconditioner:
    def __init__(self, device, velocity_hessian_weight: float):
        self._device = device
        self._velocity_hessian_weight = velocity_hessian_weight
        self._inverse_hessian = None
        self._time_inverse_hessian = None

    def precondition(self, optimized_state: PathOptimizedState):
        point_count = optimized_state.positions_.shape[0]
        if self._inverse_hessian is None:
            self._inverse_hessian = self._calculate_inv_hessian(point_count)
            self._time_inverse_hessian = self._calculate_inv_hessian(point_count + 1)
        optimized_state.positions_.grad = self._inverse_hessian @ optimized_state.positions_.grad
        optimized_state.times_.grad = self._time_inverse_hessian @ optimized_state.times_.grad

    def _calculate_inv_hessian(self, point_count):
        hessian = self._velocity_hessian_weight * self._calculate_velocity_hessian(point_count) + np.eye(point_count)
        inv_hessian = np.linalg.inv(hessian)
        return torch.tensor(inv_hessian.astype(np.float32), device=self._device)

    @staticmethod
    def _calculate_velocity_hessian(point_count):
        hessian = np.zeros((point_count, point_count), dtype=np.float32)
        for i in range(point_count):
            hessian[i, i] = 4
            if i > 0:
                hessian[i, i - 1] = -2
                hessian[i - 1, i] = -2
        return hessian


class PathOptimizer:
    def __init__(self, timer: Timer, optimizer: OptimizerWithLagrangeMultipliers, loss_builder: PathLossBuilder,
                 state_initializer: PathOptimizedStateInitializer, grad_preconditioner: GradPreconditioner):
        self._loss_builder = loss_builder
        self._optimizer = optimizer
        self._timer = timer
        self._grad_preconditioner = grad_preconditioner
        self._state_initializer = state_initializer
        self._optimized_state: Optional[PathOptimizedState] = None

    def setup(self):
        self._optimized_state = self._state_initializer.init()
        self._optimizer.setup(self._optimized_state.optimized_parameters, [self._optimized_state.velocity_constraint_multipliers, self._optimized_state.direction_constraint_multipliers])

    def step(self, collision_model):
        self._optimizer.zero_grad()
        loss = self._loss_builder.get_loss(collision_model, self._optimized_state)
        self._timer.tick("trajectory_backward")
        loss.backward()
        self._timer.tock("trajectory_backward")
        self._timer.tick("inv_hes_grad_multiplication")
        self._grad_preconditioner.precondition(self._optimized_state)
        self._timer.tock("inv_hes_grad_multiplication")
        self._timer.tick("trajectory_optimizer_step")
        self._optimizer.step()
        self._timer.tock("trajectory_optimizer_step")

    def reparametrize(self):
        with torch.no_grad():
            self._optimized_state.reparametrize()

    @property
    def result_path(self) -> ResultPathSE2:
        return self._optimized_state.result_path


class CollisionModelPointSampler:
    def __init__(self, fine_random_offset: float, course_random_offset: float, angle_random_offset: float):
        self._fine_random_offset = fine_random_offset
        self._course_random_offset = course_random_offset
        self._angle_random_offset = angle_random_offset

    def sample(self, path: ResultPathSE2) -> StampedPointArraySE2:
        positions: np.ndarray = path.stamped_positions.as_numpy()
        times = path.stamped_positions.times
        points = positions[:,1:3]
        angles = positions[:, 3]
        course_points = points + np.random.randn(*points.shape) * self._course_random_offset
        fine_points = points + np.random.randn(*points.shape) * self._fine_random_offset
        angles = np.concatenate([angles, angles], axis=0) + np.random.randn(2 * len(angles)) * self._angle_random_offset
        points = np.concatenate([course_points, fine_points], axis=0)
        times = np.concatenate([times, times], axis=0)
        positions = PointArraySE2(x=points[:,0], y=points[:,1], theta=angles)
        ret = StampedPointArraySE2(times=times, points=positions)
        return ret

class CollisionModelFactory:
    def __init__(self, parameters: ONFModelConfig):
        self._parameters = parameters

    def make_collision_model(self):
        return ONF(self._parameters)


class CollisionNeuralFieldModelTrainer:
    def __init__(self, timer: Timer, planner_task: PathPlannerTask,
                 collision_model_point_sampler: CollisionModelPointSampler,
                 optimizer: OptimizerImpl, collision_model_factory: CollisionModelFactory,
                 device: str):
        self._timer = timer
        self._collision_model_point_sampler = collision_model_point_sampler
        self._optimizer = optimizer
        self._collision_model_factory = collision_model_factory
        self._planner_task = planner_task
        self._collision_model: Optional[nn.Module] = None
        self._device = device
        self._collision_loss_function = nn.BCEWithLogitsLoss()
        self._timer = timer

    def setup(self):
        self._collision_model = self._collision_model_factory.make_collision_model()
        self._optimizer.setup(self._collision_model.parameters())

    def learning_step(self, path: ResultPathSE2):
        self._timer.tick("optimize_collision_model")
        points = self._collision_model_point_sampler.sample(path)
        self._collision_model.requires_grad_(True)
        self._optimizer.zero_grad()
        predicted_collision = self._calculate_predicted_collision(points)
        truth_collision = self._calculate_truth_collision(points)
        truth_collision = torch.tensor(truth_collision.astype(np.float32)[:, None], device=self._device)
        loss = self._collision_loss_function(predicted_collision, truth_collision)
        loss.backward()
        self._optimizer.step()
        self._collision_model.requires_grad_(False)
        self._timer.tock("optimize_collision_model")

    def _calculate_predicted_collision(self, points: StampedPointArraySE2):
        return self._collision_model(torch.tensor(points.as_numpy().astype(np.float32), device=self._device))

    def _calculate_truth_collision(self, points):
        return self._planner_task.collision_detector.is_collision(points)

    @property
    def onf_model(self) -> ONF:
        return self._collision_model

    @property
    def collision_model(self) -> nn.Module:
        return self._collision_model


class DynamicNeuralFieldOptimalPathPlanner:
    def __init__(self, planner_task: PathPlannerTask,
                 collision_neural_field_model_trainer: CollisionNeuralFieldModelTrainer, 
                 path_optimizer: PathOptimizer,
                 iterations: int, reparametrize_rate: int):
        self._reparametrize_rate = reparametrize_rate
        self._planner_task = planner_task
        self._path_optimizer = path_optimizer
        self._collision_neural_field_model_trainer = collision_neural_field_model_trainer
        self._iterations = iterations
        self._iteration = 0

    def plan(self) -> ResultPathSE2:
        self._path_optimizer.setup()
        self._collision_neural_field_model_trainer.setup()
        for i in range(self._iterations):
            self.step()
        self._path_optimizer.reparametrize()
        return self._path_optimizer.result_path

    def step(self):
        path: ResultPathSE2 = self._path_optimizer.result_path
        self._collision_neural_field_model_trainer.learning_step(path)
        collision_model = self._collision_neural_field_model_trainer.collision_model
        self._path_optimizer.step(collision_model)
        self._iteration += 1
        if self._reparametrize_rate != -1 and self._iteration % self._reparametrize_rate == 0:
            self._path_optimizer.reparametrize()

    def reparametrize(self):
        self._path_optimizer.reparametrize()

    def setup(self):
        self._path_optimizer.setup()
        self._collision_neural_field_model_trainer.setup()
    
    def get_trajectory(self) -> ResultPathSE2:
        return copy(self._path_optimizer.result_path)
    
    def get_bounds(self) -> RectangleRegionArray:
        return copy(self._collision_neural_field_model_trainer._planner_task.collision_detector.outside_rectangle_region_array)
    
    def get_map(self) -> RoadMapPolygon:
        return copy(self._collision_neural_field_model_trainer._planner_task.collision_detector.road_map.get_map())

    def get_onf_model(self) -> nn.Module:
        return copy(self._collision_neural_field_model_trainer.onf_model)