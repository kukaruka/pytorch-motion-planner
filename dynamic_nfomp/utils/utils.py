import dataclasses
from dataclasses import dataclass
import numpy as np
import torch
import torch.nn.functional
from dynamic_nfomp.utils.position2d import Position2D
import geopandas as gpd
from shapely.geometry.polygon import Polygon, Point
from copy import copy
import time
from math import sqrt
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.ops import unary_union
from shapely.validation import make_valid
from shapely.strtree import STRtree
import geopandas as gpd
import pandas
import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner/beamng')
from beamng.scene_config import SCENE, Scene

def wrap_angles(angles):
    return (angles + np.pi) % (2 * np.pi) - np.pi

def interpolate_2d_positions(positions: np.ndarray, old_times: np.ndarray, new_times: np.ndarray) -> np.ndarray:
    
    assert len(positions.shape) == 2, f"Interpolate 2D positions require rectangular array [N,3], but given shape is {positions.shape}"
    assert positions.shape[1] == 3, f"Interpolate 2D positions require rectangular array [N,3], but length of the first dimension {positions.shape[1]}"
    assert len(old_times.shape) == len(new_times.shape) == 2, f"Interpolate 2D times require rectangular array [N,1], but given shape is {old_times.shape}, {new_times.shape}"
    assert old_times.shape[1] == new_times.shape[1] == 1, f"Interpolate 2D times require rectangular array [N,1], but length of the first dimension {positions.shape[0]}"
    upper_indices: np.ndarray = np.searchsorted(old_times[:,0], new_times[:,0])[:, np.newaxis]
    lower_indices: np.ndarray = upper_indices - 1
    lower_indices = np.where(lower_indices > 0, lower_indices, 0)
    upper_indices = np.where(upper_indices < positions.shape[0], upper_indices, positions.shape[0] - 1)
    lower_old_times = np.take(old_times, lower_indices)
    upper_old_times = np.take(old_times, upper_indices)
    lower_x: np.ndarray = positions[lower_indices]
    upper_x: np.ndarray = positions[upper_indices]
    denominator = np.array(upper_old_times - lower_old_times, dtype=np.float32)
    denominator = np.where(np.abs(denominator) > 1e-3, denominator, np.full_like(denominator, 1e-3))
    weights = (new_times - lower_old_times) / denominator
    weights = weights[:, None]
    interpolated_values = (1 - weights) * lower_x + weights * upper_x
    return np.squeeze(interpolated_values)

def interpolate_1d_pytorch(x: torch.Tensor, old_times: torch.Tensor, new_times: torch.Tensor) -> torch.Tensor:   
    upper_indices: torch.Tensor = torch.searchsorted(old_times, new_times)
    lower_indices: torch.Tensor = upper_indices - 1
    lower_indices = torch.where(lower_indices > 0, lower_indices, 0)
    upper_indices = torch.where(upper_indices < x.shape[0], upper_indices, x.shape[0] - 1)
    lower_old_times = torch.gather(old_times, 0, lower_indices)
    upper_old_times = torch.gather(old_times, 0, upper_indices)
    lower_x: torch.Tensor = x[lower_indices]
    upper_x: torch.Tensor = x[upper_indices]
    denominator = upper_old_times - lower_old_times
    denominator = torch.where(torch.abs(denominator) > 1e-6, denominator, torch.full_like(denominator, 1e-6))
    weights = (new_times - lower_old_times) / denominator
    weights = weights[:, None]
    interpolated_values = (1 - weights) * lower_x + weights * upper_x
    return interpolated_values


class PointArray2D:
    def __init__(self, x, y):
        assert x.shape == y.shape
        self._x = x
        self._y = y

    def as_numpy(self):
        return np.stack([self._x, self._y], axis=-1)

    @classmethod
    def from_vec(cls, vec):
        return cls(vec[..., 0], vec[..., 1])

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def __repr__(self):
        return f"PointArray2D(x={self.x}, y={self.y})"


class PointArraySE2:
    def __init__(self, x, y, theta):
        assert x.shape == y.shape == theta.shape
        self._x = x
        self._y = y
        self._theta = theta

    def as_numpy(self):
        return np.stack([self._x, self._y, self._theta], axis=-1)

    @classmethod
    def from_vec(cls, vec):
        return cls(vec[..., 0], vec[..., 1], vec[..., 2])

    @property
    def theta(self):
        return self._theta

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def __repr__(self):
        return f"PointArray2D(x={self.x}, y={self.y}, theta={self.theta})"


def unfold_angles(angles: torch.Tensor):
    angles: torch.Tensor = wrap_angles(angles)
    delta: torch.Tensor = angles[1:] - angles[:-1]
    delta = torch.where(delta > np.pi, delta - 2 * np.pi, delta)
    delta = torch.where(delta < -np.pi, delta + 2 * np.pi, delta)
    if len(angles.shape) == 1:
        return angles[0] + torch.cat([torch.zeros(1), torch.cumsum(delta, dim=0)], dim=0)
    return angles[0, None] + torch.cat([torch.zeros(1, delta.shape[1]), torch.cumsum(delta, dim=0)],
                                       dim=0)

@dataclass
class MovingObstacle:
    y_line: np.ndarray
    width: float
    length: float
    velocity: float
    x_initial_position: float


@dataclass
class MovingObject:
    position: Position2D
    time: float

@dataclass
class StampedPointArray2D:
    times: np.ndarray
    points: PointArray2D

    def as_numpy(self):
        return np.concatenate([self.times[:, np.newaxis], self.points.as_numpy()], axis=1)

@dataclass
class StampedPointArraySE2:
    times: np.ndarray
    points: PointArraySE2

    def as_numpy(self):
        return np.concatenate([self.times[:, np.newaxis], self.points.as_numpy()], axis=1)


@dataclass
class Point2D:
    x: float
    y: float

    def as_numpy(self):
        return np.array([self.x, self.y])

@dataclass
class PointSE2:
    x: float
    y: float
    theta: float

    def as_numpy(self):
        return np.array([self.x, self.y, self.theta])


class RectangleRegionArray:
    def __init__(self, min_x: np.ndarray, max_x: np.ndarray, min_y: np.ndarray, max_y: np.ndarray):
        assert min_x.shape == max_x.shape
        assert min_x.shape == min_y.shape
        assert min_x.shape == max_y.shape
        assert np.all(min_x < max_x)
        assert np.all(min_y < max_y)
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y

    def inside(self, positions: PointArraySE2) -> np.ndarray:
        result = self.min_x <= positions.x[:, None]
        result &= positions.x[:, None] <= self.max_x
        result &= self.min_y <= positions.y[:, None]
        result &= positions.y[:, None] <= self.max_y
        return np.any(result, axis=1)

    @classmethod
    def from_dict(cls, data):
        data = np.array(data)
        assert len(data.shape) == 2
        assert data.shape[1] == 4
        return cls(data[:, 0], data[:, 1], data[:, 2], data[:, 3])

    def __len__(self):
        return len(self.min_x)

@dataclass
class PolygonMapBuilder(object):  
    def triangulate(poly: Polygon):
        points = np.vstack((poly.exterior.coords.xy[0], poly.exterior.coords.xy[1])).T
        points = points[0:-1]
        triangles = []
        right = 0
        left = len(points)-1
        pair = 0
        while right < left:
            if pair % 2 == 0:
                tri = Polygon([points[right], points[right+1], points[left]])
                right += 1
            else:
                tri = Polygon([points[left], points[right], points[left-1]])
                left -= 1
            pair += 1 
            triangles.append(make_valid(tri))
        return triangles

    def build(roads: np.ndarray, clip_rect: RectangleRegionArray):       
        j = 0
        for road in roads:
            j += len(road)
        points = np.zeros((int(2*j), 2), dtype=np.float32)
        k = 0
        polygons = gpd.GeoSeries()
        for i in range(len(roads)):
            poly = []
            tmp = k
            for j in range(len(roads[i])):
                x = roads[i][j]["right"][0]
                y = roads[i][j]["right"][1]
                points[k][0] = np.around(x, decimals=1)
                points[k][1] = np.around(y,decimals=1)
                poly.append([points[k][0], points[k][1]])
                k += 1
            tmp = []
            for j in range(len(roads[i])):
                x = roads[i][j]["left"][0]
                y = roads[i][j]["left"][1]
                points[k][0] = np.around(x,decimals=1)
                points[k][1] = np.around(y,decimals=1)
                tmp.append([points[k][0], points[k][1]])
                k += 1
            poly += tmp[::-1]
            poly = make_valid(Polygon(poly))
            list_ = []
            if poly.geom_type == 'MultiPolygon': 
                for geom in poly.geoms:
                    # list_.append(PolygonMapBuilder.triangulate(geom))
                    list_.append(geom)
            elif poly.geom_type == 'Polygon': 
                # list_.append(PolygonMapBuilder.triangulate(poly))
                list_.append(poly)
            for p in list_:
                    polygons = pandas.concat([polygons.geometry, gpd.GeoSeries(p).geometry])
                    if not p.is_valid:
                        raise ValueError("Polygon is not valid")
        polygons = polygons.clip_by_rect(xmin=clip_rect.min_x[0], ymin=clip_rect.min_y[0], xmax=clip_rect.max_x[0], ymax=clip_rect.max_y[0])
        # Get rid of empty polygons beyond the clipping
        polygons =  polygons[~polygons.is_empty]
        # Get rid of invalid shapes such as 'LineString'
        polygons = polygons[polygons.geom_type == 'Polygon']
        tree = STRtree(polygons)
        # polygons = unary_union(polygons)
        return polygons, tree

    def as_numpy(roads: gpd.GeoSeries) -> np.ndarray:
        r = []
        for poly in roads:
            p = []
            for x, y in zip(poly.exterior.coords.xy[0], poly.exterior.coords.xy[1]):
                p.append([x,y])
            r.append(p)
        return np.asarray(r, dtype=object)

@dataclass
class RoadMapPolygon:
    road_map: gpd.GeoSeries
    tree: STRtree

    def inside(self, positions: PointArraySE2, ego_length: float, ego_width: float, method="precise") -> np.ndarray:
        result = np.zeros((len(positions.x),), dtype=bool)

        if (method == "precise"):
            # Use rectangular polygon
            x = ego_width/2
            y = ego_length/2
            ego_arr = np.array([(x, y), (-x, y), (-x, -y), (x, -y)])

            for i in range(len(positions.x)):
                s = np.sin(positions.theta[i])
                c = np.cos(positions.theta[i])
                rm = np.array([[c, -s], [s, c]])
                ego = np.array([np.matmul(rm, point) for point in ego_arr])
                ego = np.add(ego, [positions.x[i], positions.y[i]])
                result[i] = (self.road_map.contains(Polygon([ego[0], ego[1], ego[2], ego[3]])).any())
                
        elif (method == "fast"):
            # Use circle approximation
            R = sqrt((ego_length/2)**2 + (ego_width/2)**2)
            for i in range(len(result)):
                result[i] = (self.road_map.contains(Point(positions.x[i], positions.y[i]).buffer(R)).any())

        elif (method == "STRtree"):
            # Use rectangular polygon
            x = ego_width/2
            y = ego_length/2
            ego_arr = np.array([(x, y), (-x, y), (-x, -y), (x, -y)])
            for i in range(len(positions.x)):
                s = np.sin(positions.theta[i])
                c = np.cos(positions.theta[i])
                rm = np.array([[c, -s], [s, c]])
                ego = np.array([np.matmul(rm, point) for point in ego_arr])
                ego = np.add(ego, [positions.x[i], positions.y[i]])
                # Get intersections of bounding boxes
                idx = self.tree.query(Polygon([ego[0], ego[1], ego[2], ego[3]]))
                geoms = self.tree.geometries.take(idx).tolist()
                result[i] = False
                for geom in geoms:
                    poly = np.vstack((geom.exterior.coords.xy[0], geom.exterior.coords.xy[1])).T
                    result[i] |= PolygonChecker.intersect(a=poly, b=ego)
                result[i] = ~result[i]
        else:
            raise ValueError(f"Bad method given in raodmap collision detector. Should be 'precise' or 'fast' but '{method}' given.")
        return result

    def get_map(self) -> gpd.GeoSeries:
        return self.road_map

    @classmethod
    def from_dict(cls, path, clip_rect:RectangleRegionArray):
        roads_np = np.load(path, allow_pickle=True)
        roadsmap, tree = PolygonMapBuilder.build(roads_np, clip_rect)
        return cls(roadsmap, tree)

@dataclass
class PolygonChecker:
    @staticmethod
    def intersect(a: np.ndarray, b: np.ndarray):
        """
        Helper function to determine whether there is an intersection between the two polygons described
        by the lists of vertices. Uses the Separating Axis Theorem
        
        Asserted with Shapely.intersects
        
        @param a an ndarray of connected points [[x_1, y_1], [x_2, y_2],...] that form a closed polygon
        @param b an ndarray of connected points [[x_1, y_1], [x_2, y_2],...] that form a closed polygon
        @return true if there is any intersection between the 2 polygons, false otherwise
        """
        polygons = [a, b]
        minA, maxA, projected, i, i1, j, minB, maxB = None, None, None, None, None, None, None, None
        for i in range(len(polygons)):
            polygon = polygons[i]
            for i1 in range(len(polygon)):
                i2 = (i1 + 1) % len(polygon)
                p1 = polygon[i1]
                p2 = polygon[i2]
                normal = { 'x': p2[1] - p1[1], 'y': p1[0] - p2[0] }
                minA, maxA = None, None
                for j in range(len(a)):
                    projected = normal['x'] * a[j][0] + normal['y'] * a[j][1]
                    if (minA is None) or (projected < minA): 
                        minA = projected
                    if (maxA is None) or (projected > maxA):
                        maxA = projected
                minB, maxB = None, None
                for j in range(len(b)): 
                    projected = normal['x'] * b[j][0] + normal['y'] * b[j][1]
                    if (minB is None) or (projected < minB):
                        minB = projected
                    if (maxB is None) or (projected > maxB):
                        maxB = projected
                if (maxA < minB) or (maxB < minA):
                    return False
        return True

@dataclass
class MovingObstacle2DPolygon:
    positions: np.ndarray
    times: np.ndarray
    length: float
    width: float

    def inside(self, poses: StampedPointArraySE2, ego_length: float, ego_width: float, method="precise") -> np.ndarray:

        positions = interpolate_2d_positions(self.positions, self.times+2, poses.times.reshape((len(poses.times), 1)))
        result = np.zeros((len(positions),), dtype=bool)
        if (method == "precise"):
            # Use rectangular polygons
            x = ego_width/2
            y = ego_length/2
            ego_arr = np.array([(x, y), (-x, y), (-x, -y), (x, -y)])
            x = self.width/2
            y = self.length/2
            obs_arr = np.array([(x, y), (-x, y), (-x, -y), (x, -y)])

            for i in range(len(result)):
                s = np.sin(positions[i][2])
                c = np.cos(positions[i][2])
                rm = np.array([[c, -s], [s, c]])
                obs = np.array([np.matmul(rm, point) for point in obs_arr])
                obs = np.add(obs, [positions[i][0], positions[i][1]])
                s = np.sin(poses.points.theta[i])
                c = np.cos(poses.points.theta[i])
                rm = np.array([[c, -s], [s, c]])
                ego = np.array([np.matmul(rm, ego_point) for ego_point in ego_arr])
                ego = np.add(ego, [poses.points.x[i], poses.points.y[i]])
                result[i] = PolygonChecker.intersect(a=([obs[0], obs[1], obs[2], obs[3]]), b=([ego[0], ego[1], ego[2], ego[3]]))
            # print(len(poses.times[result]))

        elif (method == "fast"):
                # Use circle approximations
                R1 = sqrt((ego_length/2)**2 + (ego_width/2)**2)
                R2 = sqrt((self.length/2)**2 + (self.width/2)**2)
                for i in range(len(result)):
                    dx = poses.points.x[i] - positions[i][0]
                    dy = poses.points.y[i] - positions[i][1]
                    dist = sqrt(dx*dx + dy*dy)
                    result[i] = (dist <= R1+R2)
        else:
            raise ValueError(f"Bad method given in moving obstacle collision detector. Should be 'precise' or 'fast' but '{method}' given.")
        return result

    @classmethod
    def from_dict(cls, data):
        width = float(data['width'])
        length = float(data['length'])
        positions = np.array(data['positions'])
        times = np.array(data['times']).reshape((len(data['times']),1))
        return cls(positions, times, length, width)

    def __len__(self):
        return len(self.times)


@dataclass
class MovingObstacleArray1D:
    y_line: np.ndarray
    width: np.ndarray
    length: np.ndarray
    velocity: np.ndarray
    x_initial_position: np.ndarray

    def inside(self, positions: StampedPointArray2D) -> np.ndarray:
        min_x = self.min_x(positions.times)
        min_y = self.min_y(positions.times)
        max_x = self.max_x(positions.times)
        max_y = self.max_y(positions.times)
        result = min_x <= positions.points.x[:, None]
        result &= positions.points.x[:, None] <= max_x
        result &= min_y <= positions.points.y[:, None]
        result &= positions.points.y[:, None] <= max_y
        return np.any(result, axis=1)

    def min_x(self, times):
        x_positions = self.x_initial_position[np.newaxis, :] + self.velocity * times[:, np.newaxis]
        result = x_positions - self.length[np.newaxis, :] / 2
        return result

    def max_x(self, times):
        x_positions = self.x_initial_position[np.newaxis, :] + self.velocity * times[:, np.newaxis]
        result = x_positions + self.length[np.newaxis, :] / 2
        return result

    def min_y(self, times):
        result = self.y_line[np.newaxis, :] - self.width[np.newaxis, :] / 2
        return np.repeat(result, len(times), axis=0)

    def max_y(self, times):
        result = self.y_line[np.newaxis, :] + self.width[np.newaxis, :] / 2
        return np.repeat(result, len(times), axis=0)

    @classmethod
    def from_dict(cls, data):
        y_line = np.array(data['y_line'])
        width = np.array(data['width'])
        length = np.array(data['length'])
        velocity = np.array(data['velocity'])
        x_initial_position = np.array(data['x_initial_position'])
        return cls(y_line, width, length, velocity, x_initial_position)

    def __len__(self):
        return len(self.x_initial_position)

@dataclasses.dataclass
class ResultPath2D:
    stamped_positions: StampedPointArray2D

@dataclasses.dataclass
class ResultPathSE2:
    stamped_positions: StampedPointArraySE2
    
    def as_beamng_script(self) -> dict:
        script = []
        points = self.stamped_positions.as_numpy()
        z = 0
        for i in range(0,len(points)):

            if SCENE == Scene.cross:
                if (i < 55):
                    z = 92.
                else:
                    z = 999.  
            elif SCENE == Scene.hotel:
                if (i < 999):
                    z = 999.
                else:
                    z = 999.  
            elif SCENE == Scene.harbor:
                if(i < 999):
                    z = 80.
                else:
                    z = 80.   
            else:
                z = 0.0
            script.append({'x':float(points[i][1]),
                            'y':float(points[i][2]),
                            'z':float(z),
                            't':float(points[i][0]),
                            'theta':float(points[i][3])})

        # script.append({'x':float(points[-1][1]),
        #                     'y':float(points[-1][2]),
        #                     'z':float(z),
        #                     't':float(points[-1][0]),
        #                     'theta':float(points[-1][3])})
        return script
    
    def as_numpy(self) -> np.ndarray:
        # [N,4]
        # [time, x, y, theta]
        return self.stamped_positions.as_numpy()

@dataclasses.dataclass
class OptimizerImplConfig:
    lr: float
    beta1: float
    beta2: float