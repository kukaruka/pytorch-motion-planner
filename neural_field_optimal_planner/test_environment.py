class TestEnvironment(object):
    def __init__(self, start_point, goal_point, bounds, obstacle_points):
        self.startPoint = start_point
        self.goalPoint = goal_point
        self.bounds = bounds
        self.obstaclePoints = obstacle_points
