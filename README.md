# Pytorch motion planner
## Description
This repository contains code for Neural field optimal path planner (NFOPP)

The idea is to construct Optimal path planner with Laplacian regularization. 
For optimization of absence of obstacles, it is possible to use 
Neural field which predict probability of collision for given position. 
Thus, loss function consists of three part: collision loss function
which minimize position in collision, Laplacian regularization which 
minimize path length and total rotation, and allow optimizing smooth
trajectory, and constraint loss which forces position to specific constraint. 
Constraint loss can be done with Lagrangian multipliers

[![Dynamic NFOMP with BeamNG.tech](http://img.youtube.com/vi/yaeI4JM2-OI/0.jpg)](http://www.youtube.com/watch?v=yaeI4JM2-OI "Dynamic Neural Field Optimal motion Planner demo")

Dynamic Neural Field Optimal motion Planner demo

### Planner is capable

- Make optimal point to point paths which combine rotation and translation loss
- Make non-holonomic optimal paths
- Make non-holonomic with only forward movement paths
- Make paths in corridor with obstacles
- Make paths in corridor with obstacles with non-holonomic only forward movement
- Make paths in environment with random obstacles 

### Main features

- Laplacian's regularization for points on path as distance loss term (CHOMP loss term)
- Neural field representation for collision loss which can generate smooth gradients
- Continuous simultaneous learning for collision neural field with path points
- Lagrangian's multipliers as loss term for learning strict and not-strict constraints
- Quadratic loss term for constraints for fast initial convergence (with Lagrangian multipliers)
- Trajectory re-parametrization

# Prerequisites

**BeamNG.tech**
[Link](https://beamng.tech/) v0.27.0.0 or older

You should acquire your own license key for educational purposes.

**Python 3.9**

**Python modules**
- pytorch 1.9.0+cu111
- numpy
- matplotlib
- PyYAML
- Shapely

```bash
pip3 install torch torchvision torchaudio
pip3 install pytorch-lightning
pip3 install numpy matplotlib pyyaml scipy shapely
```

# Running

1. Open `sim.ipynb` and run first two cells.
The sim should be running after it with the specified scene in the West Coast map.

2. Run dnfomp/main.py. Wait for the planner to build the trajectory. The trajectory and accelerationas are dumped to corresponding files.

To adjust the planner parameters, see `beamng/planner.py`, `beamng/scene_config.py`.

# Notes

When plotting heatmap on the plot it can overlay the other objects. This is because of matplotlib `computed_zorder` method on 3d-axis plots (actually they are 2d but nevermind). 

You may modify yout matplotlib lib in `/mplot3d/axes3d.py`, line `474`:
```python
for artist in sorted(collections_and_patches,
                     key=lambda artist: artist.do_3d_projection(),
                     reverse=True):
      if isinstance(artist, mcoll.Collection):
         if (artist.zorder >= 0): # <---- added a condition here
            artist.zorder = collection_zorder
            collection_zorder += 1

      elif isinstance(artist, mpatches.Patch):
         artist.zorder = patch_zorder
         patch_zorder += 1
```

That way you force objects with negative `zorder` to always be projected to the background, while preserving right projection order for the rest of the objects.
