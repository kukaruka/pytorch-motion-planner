
import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner/dynamic_nfomp')
import numpy as np
from dynamic_nfomp.utils.timer import Timer
from dynamic_nfomp.utils.universal_factory import UniversalFactory
from dynamic_nfomp.collision_detector.polygon_collision_detector import MovingPolygonCollisionDetector
from dynamic_nfomp.dynamic_nfomp import *
from sim_interface import SimInterface as SimInterface
import numpy as np
from multiprocessing import Queue, Event
import queue
import time
from pytorch_lightning.utilities import AttributeDict
from scene_config import Scene

from scene_config import SCENE, Scene


polygon_collision_detector_parameters = {
    'moving_obstacle_array': {
        'width': 3.,
        'length': 12.,
        'positions': [],
        'times': [],
    },
    "ego_length" : 1.0,
    "ego_width" : 1.0,
    "collision_method" : "precise",
    "outside_rectangle_region_array": SCENE.bounds,
    "road_map_path" : "beamng/roads.npy"
}

trajectory_initializer_parameters = {
    'resolution': 5
}

planner_parameters = {
    "start" : SCENE.start_point,
    "goal": SCENE.goal_point,
    "max_velocity": 8.0,
    "collision_neural_field_model_trainer": {
        "collision_model_point_sampler": {
            "course_random_offset": 1.,
            "fine_random_offset": 3.,
            "angle_random_offset": 0.1,
        },
        "optimizer": {
            "lr": 1e-1,
            "beta1": 0.9,
            "beta2": 0.9
        },
        "collision_model_factory": {
            "mean": 0,
            "sigma": 8,
            "input_dimension": 4,
            "encoding_dimension": 128,
            "hidden_dimensions": [128, 128, 128]
        }
    },
    "path_optimizer": {
        "optimizer": {
            "lr": 2e-2,
            "beta1": 0.9,
            "beta2": 0.9,
            "lagrange_multiplier_lr": 1e-2,
            "base_lr": 2e-2,
            "max_lr": 1e-1,
            "step_size_up": 10,
            "step_size_down": 10,
        },
        "loss_builder": {
            "distance_weight": 5e1,
            "collision_weight": 5e4,
            "time_regularization_weight": 1e2,
            "soft_maximal_velocity_constraint_weight": 1e2,
            "direction_constraint_weight": 5e1,
            "second_differences_weight": 5e1

        },
        "state_initializer": {
            "path_state_count": 80,
        },
        "grad_preconditioner": {
            "velocity_hessian_weight": 5e0,
        }
    },
    "iterations": 400,
    "reparametrize_rate": 10,
}

class Planner:
    def __init__(self, sim: SimInterface, qCarPoses: dict, qTraj: dict, qModel: Queue, qEnvironment: Queue, qScript: dict, run_flag: Event):
        
        self.ready = False
        self.log = False
        self.run = run_flag
        self.traj = None
        self.sim = sim
        self.carPoses = dict()
        self.carNames = sim.getCurrentCarNames()
        
        self.qCarPoses = qCarPoses
        print("[Planner] Waiting for data in queues")
        for car in self.qCarPoses:
            self.carPoses[car] = qCarPoses[car].get(block=True)

        self.qTraj = qTraj
        self.qEnvironment = qEnvironment
        self.qModel = qModel        # TODO уточнить тип объекта в плотере
        self.qScript = qScript

        print("[Planner] Start making environment")
        self.dummy = SCENE.dummy_script
        polygon_collision_detector_parameters['moving_obstacle_array']['positions'] =  [[pose['x'], pose['y'], pose['theta']] for pose in self.dummy]
        polygon_collision_detector_parameters['moving_obstacle_array']['times'] =  [pose['t'] for pose in self.dummy]
        collision_detector = MovingPolygonCollisionDetector.from_dict(polygon_collision_detector_parameters)
        trajectory_initializer = AstarTrajectoryInitializer(collision_detector, trajectory_initializer_parameters['resolution'])

        s = planner_parameters["start"]
        g = planner_parameters["goal"]
        input_planner_task = PathPlannerTask(
            start=PointSE2(s[0], s[1], s[2]),
            goal=PointSE2(g[0], g[1], g[2]),
            collision_detector=collision_detector,
            start_time=0,
            maximal_velocity=planner_parameters["max_velocity"]
        )

        global_timer = Timer()
        device_parameter = "cpu"
        self.planner = UniversalFactory().make(DynamicNeuralFieldOptimalPathPlanner, planner_parameters,
                                                planner_task=input_planner_task, timer=global_timer,
                                                device=device_parameter, trajectory_initializer=trajectory_initializer)
        self.ready = True
        print("[Planner] Initialized")

    def process(self):
        
        self.planner.setup()
        
        bounds: RectangleRegionArray = self.planner.get_bounds()
        map: gpd.GeoSeries = self.planner.get_map()
        env = {
            "bounds": [bounds.min_x, bounds.max_x, bounds.min_y, bounds.max_y],
            "static_obstacles": map
        }
        self.qEnvironment.put(env)
        
        print("[Planner] Started")
        start = time.time()
        for i in range(planner_parameters["iterations"]):
            # For continuous replanning
            # for name in self.carNames:
            #     try:
            #         self.carPoses[name] = self.qCarPoses[name].get(block=False)
            #     except queue.Empty:
            #         continue
            self.planner.step()
            if (i % 1 == 0):
                try:
                    model = self.planner.get_onf_model()
                    self.qModel.put(model, block=False)
                except queue.Full:
                    pass
                traj = self.planner.get_trajectory()

                try:
                    self.qTraj["plotter"].put(traj.as_numpy(), block=False)
                except queue.Full:
                    pass

                try:
                    self.qTraj["sim"].put(traj.as_numpy(), block=False)
                except queue.Full:
                    pass
                    
        self.planner.reparametrize()

        print(f"[Planner] Run task time: {time.time() - start} seconds")

        print(f"[Planner] Sending dummy script to sim")
        self.qScript["dummy"].put(self.dummy)
        print(f"[Planner] Sending ego script to sim")
        self.qScript["ego"].put(self.planner.get_trajectory().as_beamng_script())
        name = f"trajectory_{time.time()}.npy"
        np.save(name,self.planner.get_trajectory().as_numpy())
        print(f"Saved trajectory as {name}")
        while (not self.run.is_set()):
            time.sleep(1)

        print("[Planner] Cleanup")
        print("[Planner] Shutdown")