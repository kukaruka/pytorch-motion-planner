import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/astar')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/utils')

from planner_utils import Pose, Point, Quaternion
from sim_interface import SimInterface
import numpy as np
from multiprocessing import Queue, Event
import queue
import string
import time
from scene_config import SCENE, Scene

class Sim():
    def __init__(self, sim: SimInterface, qCarPoses: dict, qTraj: Queue, qScripts: dict, run_flag: Event):
        self.log = False
        self.sim = sim
        self.carNames = sim.getCurrentCarNames()
        self.run = run_flag
        self.qCarPoses = qCarPoses
        self.qTraj = qTraj
        self.qScripts = qScripts
        self.scripts = {
            "ego": None,
            "dummy" : None
        }

        self.carPoses = dict()
        self.updateVehiclePoses()
        for car in self.qCarPoses:
            self.qCarPoses[car].put(self.carPoses[car])
        try:
            self.traj = qTraj.get(block=False)
        except queue.Empty:
            self.traj = None
        self.sphere_ids = []
        self.ego_imu_dump = []
        self.imu_dump_len = 0
        self.start_collecting = False
        self.start_collecting_time = 0
        self.imu_readings = []
        self.ego_pose_dump = []
        print("[Sim] Initialized")

    def process(self):
        while (not self.run.is_set()):

            self.imu_readings.append(self.sim.poll_ego_imu()) if self.start_collecting else None
            self.updateVehiclePoses()
            self.imu_readings.append(self.sim.poll_ego_imu()) if self.start_collecting else None
            self.sendTrajToSim()
            self.imu_readings.append(self.sim.poll_ego_imu()) if self.start_collecting else None
            self.sendScriptsToSim()
            self.imu_readings.append(self.sim.poll_ego_imu()) if self.start_collecting else None
            for car in self.carNames:
                try:
                    self.qCarPoses[car].put(self.carPoses[car], block=False)
                except queue.Full:
                    pass
            try:
                self.traj = self.qTraj.get(block=False)
            except queue.Empty:
                pass

            try:
                self.scripts["ego"] = self.qScripts["ego"].get(block=False)
                self.scripts["dummy"] = self.qScripts["dummy"].get(block=False)
                
            except queue.Empty:
                pass
            if (self.start_collecting):
                self.collect_imu_data()
                self.collect_pose_data()
            print("[Sim] Running") if self.log else None
        print("[Sim] Cleanup")
        self.sim.remove_all_debug_graphics()
        self.sim.remove_ego_imu()
        print("[Sim] Shutdown")

    def collect_pose_data(self):
        pos, _ = self.sim.get_vehicle_pose(self.sim.get_current_vehicles()["ego"])
        self.ego_pose_dump.append([pos[0], pos[1], time.time() - self.start_collecting_time])
        if (time.time() - self.start_collecting_time > 60.0):
            self.start_collecting = False
            fname = f"pose_dump_{time.time()}.npy"
            print(f"Saving to {fname}")
            np.save(fname, np.array(self.ego_pose_dump))

    def collect_imu_data(self):
        self.imu_readings.append(self.sim.poll_ego_imu())
        for i in range(len(self.imu_readings)):
            for bulk in self.imu_readings[i]:
                if len(bulk) == 0:
                    continue
                self.ego_imu_dump.append([bulk['accSmooth'][0],                 # The reading in the IMU's local x-axis.
                bulk['accSmooth'][1],              # The reading in the IMU's local y-axis.
                bulk['accSmooth'][2],                 # The reading in the IMU's local z-axis.
                bulk['time']])         
        if (time.time() - self.start_collecting_time > 45.0):
            self.start_collecting = False
            fname = f"imu_dump_{time.time()}.npy"
            print(f"Saving to {fname}")
            np.save(fname, np.array(self.ego_imu_dump))
        self.imu_readings = []

    def sendScriptsToSim(self):
        if (self.scripts["ego"] is not None):
            self.sim.sendScript("ego", self.scripts["ego"])
            self.scripts["ego"] = None
            self.start_collecting = True
            self.start_collecting_time = time.time()
            print("Start collecting")
        if (self.scripts["dummy"] is not None):
            self.sim.sendScript("dummy", self.scripts["dummy"])
            self.scripts["dummy"] = None
        
        

    def updateVehiclePoses(self) -> dict:
        for car in self.carNames:
            p, q = self.sim.get_vehicle_pose(self.sim.vehicles[car])
            # v = self.sim.get_vehicle_velocity(self.sim.vehicles[vehicle])
            pose = Pose(position=Point(x=p[0], y=p[1], z=p[2]),
                        orientation=Quaternion(x=q[0], y=q[1], z=q[2], w=q[3]))
            self.carPoses[car] = pose

    def sendTrajToSim(self):
        if (self.traj is not None):
            points = np.vstack([self.traj[:,1], self.traj[:,2], np.zeros_like(self.traj[:,2])]).T

            if SCENE == Scene.cross:
                for i in range(50):
                    points[i][2] = 92.
                for i in range(50,len(points)):
                    points[i][2] = 999. 

            elif SCENE == Scene.hotel:
                for i in range(60):
                    points[i][2] = 999.
                for i in range(60,len(points)):
                    points[i][2] = 999.   

            elif SCENE == Scene.harbor:
                for i in range(60):
                    points[i][2] = 80.
                for i in range(60,len(points)):
                    points[i][2] = 80. 
            else:
                for i in range(len(points)):
                    points[i][2] = 0. 

            self.sim.plot_debug_polylines(points)
            self.sphere_ids.append(self.sim.plot_debug_spheres(points))
            if (len(self.sphere_ids) == 2):
                self.sim.remove_debug_spheres(self.sphere_ids[0])
                del self.sphere_ids[0]
            self.sim.remove_debug_polylines(self.sim.debug_poly_ids)
            