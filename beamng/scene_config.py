from dataclasses import dataclass
from pytorch_lightning.utilities import AttributeDict
import numpy as np
import os

@dataclass
class Scene():
    cross = AttributeDict(
        name = "cross",
        ego_pose=AttributeDict(
            pos=[-567.24, 720.19, 81.63],
            rotation=[0.034, 0.072, 0.934, -0.346]
        ),
        dummy_pose=AttributeDict(
            pos=[-684.42,661.72,96.49],
            rotation=[0.053, -0.05, 0.71, 0.69]
        ),
        dummy_script = np.load("C:/Users/user/Documents/pytorch-motion-planner/beamng/dummy_script_cross1.npy", allow_pickle=True),
        start_point = [-567.24, 720.19, 0.7],
        goal_point = [-643.19, 491.47, 0.7],
        bounds = [[-745, -435, 440, 750]]
    )
    hotel = AttributeDict(
        name = "hotel",
        ego_pose=AttributeDict(
            pos=[-416.85, 218.70, 100.91],
            rotation=[0.005,  -0.011,  0.970,  -0.245]
        ),
        dummy_pose=AttributeDict(
            pos=[-583.48, 199.26, 110.14],
            rotation=[0.028,  -0.084,  0.919,  0.383]
        ),
        dummy_script = np.load("C:/Users/user/Documents/pytorch-motion-planner/beamng/dummy_script_hotel.npy", allow_pickle=True),
        start_point = [-416.85, 218.70, 100.91],
        goal_point = [-511.43, 307.20, 98.66],
        bounds = [[-700, -350, 100, 350]]
    )
    harbor = AttributeDict(
        name = "harbor",
        ego_pose=AttributeDict(
            pos=[-191.02, 501.38, 74.99],
            rotation=[-0.005,  0.004,  0.693,  -0.721]
        ),
        dummy_pose=AttributeDict(
            pos=[-318.26, 496.50, 82.22],
            rotation=[-0.078,  0.038,  -0.380,  -0.921]
        ),
        dummy_script = np.load("C:/Users/user/Documents/pytorch-motion-planner/beamng/dummy_script_harbor.npy", allow_pickle=True),
        start_point = [-191.02, 501.38, 74.99],
        goal_point = [-318.83, 660.19, 74.97],
        bounds = [[-400, -100, 450, 750]]
    )

SCENE = Scene.cross
