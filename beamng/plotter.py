import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/astar')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/utils')

from multiprocessing import Queue, Event
from neural_field_optimal_planner.onf_model import ONF
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.collections import QuadMesh
from matplotlib.patches import Polygon
import torch
import time
import queue
import os
import torch.nn as nn
import geopandas as gpd
from shapely.geometry import mapping
from shapely.geometry.polygon import Polygon
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.linestring import LineString
from mpl_toolkits.mplot3d.art3d import Line3DCollection, Patch3DCollection, Poly3DCollection, Patch3D    
from scene_config import Scene
from pytorch_lightning.utilities import AttributeDict

class Plotter:

    def __init__(self, name, qModel: Queue, qTraj: Queue, qEnvironment: Queue, run_flag: Event, scene: AttributeDict):
        self.name = name
        self.log = True
        self.qModel: Queue = qModel
        self.qTraj: Queue = qTraj
        self.qEnvironment: Queue = qEnvironment
        self.scene = scene
        self.brickLength = 6  # x, m
        self.brickWidth = 15  # y, m
        self.brickHeight = 1  # t, sec
        self.run = run_flag
        self.model = None
        self.trajectory = None # Canvas
        self.trajectory_boxes = None
        self.collision_boxes = None
        self.obstacle_trajectory = None # Canvas
        self.obstacle_trajectory_boxes = None
        self.obstacles = None # Canvas
        self.staticObstacles = None
        self.trajectoryPoints = None
        self.flags = {"polygon_map": False, "obstacle_path": False}
        print(f"[Plotter] [{self.name}] Created")
        
    def process(self):
        while(1):
            try:
                self.model = self.qModel.get(block=False)
                self.trajectoryPoints = self.qTraj.get(block=False)
                self.environment = self.qEnvironment.get(block=False)
                break
            except queue.Empty:
                time.sleep(1)
                print(f"[Plotter] [{self.name}] Waiting for data to plot . . .")
                continue

        self.make_figure()
        print(f"[Plotter] [{self.name}] Running ")

        while (not self.run.is_set()):
            self.update_plotter_data()
            self.update_canvas_objects()
            self.drawnow()
        
        print("[Plotter] Cleanup")
        plt.close()
        print("[Plotter] Shutdown")

    def make_figure(self):
        self.bounds = self.environment['bounds']
        self.staticObstacles = self.environment['static_obstacles']
        xmin = self.bounds[0]
        xmax = self.bounds[1]
        ymin = self.bounds[2]
        ymax = self.bounds[3]
        zmin = 0
        zmax = 100

        self.fig_3d = plt.figure(dpi=150)
        self.fig_3d.add_axes([0.02, 0.02, 0.96, 0.96], projection='3d',  computed_zorder=True)
        self.ax_3d = self.fig_3d.axes[0]
        self.ax_3d.set_proj_type('persp', focal_length=0.4)
        self.ax_3d.set_xlim(xmin, xmax)
        self.ax_3d.set_ylim(ymin, ymax)
        # self.ax_3d.set_aspect("equal")
        self.ax_3d.set_zlim(zmin, zmax)
        self.ax_3d.set_xlabel(xlabel="Y [m]", fontsize=8.0)
        self.ax_3d.set_ylabel(ylabel="X [m]", fontsize=8.0)
        self.ax_3d.set_zlabel(zlabel="t [sec]", fontsize=8.0)
        self.ax_3d.view_init(azim=45, elev=45)

        self.heatmap: QuadMesh = None
        self.gridsize = 150
        self.grid_x, self.grid_y = np.meshgrid(np.linspace(xmin, xmax, self.gridsize),
                                     np.linspace(ymin, ymax, self.gridsize))
        self.grid = np.stack(
            [self.grid_x, self.grid_y, np.zeros_like(self.grid_x), np.full_like(self.grid_x, 10)], axis=2).reshape(-1, 4)
        self.mesh = self.grid.reshape(self.gridsize, self.gridsize, 4)


    def drawnow(self):
        plt.draw()
        plt.pause(0.001)
        # self.fig.show()
        # self.fig.canvas.draw()
        # self.fig.canvas.flush_events()

    def update_plotter_data(self):
        if self.qModel.empty():
            pass
        else:
            self.model = self.qModel.get()
            assert isinstance(
                self.model, nn.Module), f"[Plotter] [{self.name}] Type error in model queue assertion"

        if self.qTraj.empty():
            pass
        else:
            self.trajectoryPoints = self.qTraj.get()
            assert isinstance(
                self.trajectoryPoints, np.ndarray), f"[Plotter] [{self.name}] Type error in trajectory queue assertion"

    def update_canvas_objects(self):
        self.plot_trajectory()
        self.plot_obstacles_path()
        self.plot_polygons()
        # self.plot_collisions()
        # self.plot_model_heatmap()

    def get_heatmap(self) -> QuadMesh:
        if self.heatmap is not None:
            return self.heatmap

    def plot_model_heatmap(self, device="cpu"):
        obstacle_probabilities = np.zeros((self.gridsize*self.gridsize, 1), dtype=np.float32)
        # for i in range(len(self.trajectoryPoints)):
        self.grid = np.stack([self.grid_x, self.grid_y, np.zeros_like(self.grid_x), np.full_like(self.grid_x, 0)], axis=2).reshape(-1, 4)
        prob = self.model(torch.tensor(self.grid.astype(np.float32), device=device))
        obstacle_probabilities += prob.cpu().detach().numpy()
        obstacle_probabilities /= len(self.trajectoryPoints)
        cmap = mpl.cm.get_cmap('Blues')
        fcolors = cmap([1-prob for prob in obstacle_probabilities.reshape(self.gridsize, self.gridsize)])
        if (self.heatmap is not None):
            self.heatmap.remove()
        self.heatmap = self.ax_3d.plot_surface(
                X=np.array(self.mesh[:, :, 0]),
                Y=np.array(self.mesh[:, :, 1]),
                Z=np.zeros_like(self.mesh[:,:,0]),
                facecolors=fcolors,
                alpha=1.0,
                zorder = -1000)
        
    def plot_trajectory(self):
        points = self.trajectoryPoints
        
        z = points[:,0]
        x = points[:,1]
        y = points[:,2]
        t = points[:,3]

        if (self.trajectory is not None):
            self.trajectory.remove()
        self.trajectory = self.ax_3d.scatter(x, y, z, color="yellow", edgecolors="black",
                                        linewidths=0.5, alpha=1.0, s=4, zorder = 1000)
        w = float(self.brickWidth/2)
        l = float(self.brickLength/2)
        h = float(self.brickHeight/2)
        #vertices
        v = [[-w, -l, -h],[w, -l, -h],[w, l, -h],[-w, l, -h],
                [-w, -l, h],[w, -l, h],[w, l, h],[-w, l, h]]

        x = x[::3]
        y = y[::3]
        z = z[::3]
        t = t[::3]

        series_list = [gpd.GeoSeries(MultiPolygon([Polygon([v[0],v[1], v[2], v[3]]),
                                            Polygon([v[4],v[5], v[6], v[7]]),
                                            Polygon([v[0],v[4], v[7], v[3]]),
                                            Polygon([v[0],v[1], v[5], v[4]]),
                                            Polygon([v[1],v[2], v[6], v[5]]),
                                            Polygon([v[2],v[3], v[7], v[6]])
                                            ]))
                                            .translate(x_, y_, z_)
                                            .rotate(t_, origin="center", use_radians=True)
                                            for z_, x_, y_, t_ in zip(z,x,y,t)]


        pcs = []
        for geoseries in series_list:
            vertices = [list(x.exterior.coords) for x in geoseries[0].geoms]
            pc = []
            for plane in vertices:
                pc.append(Poly3DCollection(np.array([plane]), facecolors="deepskyblue", edgecolors="black", linewidths=0.5, alpha = 0.6, zorder = 1000))
            pcs.append(pc)
        if self.trajectory_boxes is not None:
            [[poly.remove() for poly in pc] for pc in self.trajectory_boxes]
        self.trajectory_boxes = [[self.ax_3d.add_collection(poly) for poly in pc] for pc in pcs]
            

    def plot_obstacles(self):
        points = self.staticObstacles
        if (points is not None):
            if (self.obstacles is None):
                self.obstacles = self.ax_3d.scatter(points[:, 0], points[:, 1], zs=0, zdir='z', color="black", s=1)
            else:
                self.obstacles.set_offsets(points)
    
    def plot_polygons(self):
        if (self.flags["polygon_map"]):
            return

        from shapely.ops import unary_union
        self.staticObstacles = unary_union(self.staticObstacles)
        if (isinstance(self.staticObstacles, Polygon)):
            self.staticObstacles = MultiPolygon([self.staticObstacles])
        polys_ext = [elem for elem in [list(x.exterior.coords) for x in self.staticObstacles.geoms]]
        polys_int = [elem for elem in [list([i.coords for i in x.interiors]) for x in self.staticObstacles.geoms]]

        pc = []
        for line in polys_ext:
            line = np.array([[p[0], p[1], 0.] for p in line])
            pc.append(Poly3DCollection(np.expand_dims(line, axis=0), facecolors=(0.,0.,0.,0.), edgecolors="black", linewidths=1.5, zorder = -100))
        for poly in polys_int:
            for line in poly:
                line = np.array([[p[0], p[1], 0.] for p in line])
                pc.append(Poly3DCollection(np.expand_dims(line, axis=0), facecolors=(0.,0.,0.,0.), edgecolors="black", linewidths=1.5, zorder = -100))
        [self.ax_3d.add_collection(polyline) for polyline in pc]
        self.flags["polygon_map"] = True
    
    def plot_collisions(self):
        raise NotImplementedError
        points = self.trajectoryPoints
        z = points[:,0]
        x = points[:,1]
        y = points[:,2]
        t = points[:,3]
        obstacle_probabilities = np.zeros((len(self.trajectoryPoints), 1), dtype=np.float32)
        prob = self.model(torch.tensor(self.trajectoryPoints.astype(np.float32), device="cpu")).cpu().detach().numpy()
        cmap = mpl.cm.get_cmap('Blues')
        fcolors = cmap([1-prob for prob in obstacle_probabilities.reshape(self.gridsize, self.gridsize)])

        z = points[:,0]
        x = points[:,1]
        y = points[:,2]
        w = float(10/2)
        l = float(10/2)
        h = float(1/2)
        #vertices
        v = [[-w, -l, -h],[w, -l, -h],[w, l, -h],[-w, l, -h],
                [-w, -l, h],[w, -l, h],[w, l, h],[-w, l, h]]
        
        series_list = [gpd.GeoSeries(MultiPolygon([Polygon([v[0],v[1], v[2], v[3]]),
                                            Polygon([v[4],v[5], v[6], v[7]]),
                                            Polygon([v[0],v[4], v[7], v[3]]),
                                            Polygon([v[0],v[1], v[5], v[4]]),
                                            Polygon([v[1],v[2], v[6], v[5]]),
                                            Polygon([v[2],v[3], v[7], v[6]])
                                            ]))
                                            .translate(x_, y_, z_)
                                            for z_, x_, y_, t_ in zip(z,x,y)]
        pcs = []
        for geoseries in series_list:
            vertices = [list(x.exterior.coords) for x in geoseries[0].geoms]
            pc = []
            for plane in vertices:
                pc.append(Poly3DCollection(np.array([plane]), facecolors="magenta", edgecolors="black", linewidths=0.5, alpha = 0.6, zorder = 1000))
            pcs.append(pc)
        if self.collision_boxes is not None:
            [[poly.remove() for poly in pc] for pc in self.collision_boxes]
        self.collision_boxes = [[self.ax_3d.add_collection(poly) for poly in pc] for pc in pcs]

    def plot_obstacles_path(self):
        if (self.flags["obstacle_path"]):
            return
        points = np.vstack([[pose['x'], pose['y'], pose['theta']] for pose in self.scene.dummy_script])
        heights = np.vstack([pose['t'] for pose in self.scene.dummy_script])

        x = [pose['x'] for pose in self.scene.dummy_script]
        y = [pose['y'] for pose in self.scene.dummy_script]
        t = [pose['theta'] for pose in self.scene.dummy_script]
        z = [pose['t'] for pose in self.scene.dummy_script]

        if (self.obstacle_trajectory is not None):
            self.obstacle_trajectory.remove()
        self.obstacle_trajectory = self.ax_3d.scatter(np.array(points[:, 0]),
                                                np.array(points[:, 1]),
                                                np.array(heights),
                                                color="magenta",
                                                edgecolors="black",
                                                linewidths=0.5,
                                                alpha=1.0, 
                                                s=4,
                                                zorder = 1000)

        w = float(13/2)
        l = float(6/2)
        h = float(1/2)
        #vertices
        v = [[-w, -l, -h],[w, -l, -h],[w, l, -h],[-w, l, -h],
                [-w, -l, h],[w, -l, h],[w, l, h],[-w, l, h]]

        x = x[::3]
        y = y[::3]
        z = z[::3]
        t = t[::3]

        series_list = [gpd.GeoSeries(MultiPolygon([Polygon([v[0],v[1], v[2], v[3]]),
                                            Polygon([v[4],v[5], v[6], v[7]]),
                                            Polygon([v[0],v[4], v[7], v[3]]),
                                            Polygon([v[0],v[1], v[5], v[4]]),
                                            Polygon([v[1],v[2], v[6], v[5]]),
                                            Polygon([v[2],v[3], v[7], v[6]])
                                            ]))
                                            .translate(x_, y_, z_)
                                            .rotate(t_, origin="center", use_radians=True)
                                            for z_, x_, y_, t_ in zip(z,x,y,t)]

        pcs = []
        for geoseries in series_list:
            vertices = [list(x.exterior.coords) for x in geoseries[0].geoms]
            pc = []
            for plane in vertices:
                pc.append(Poly3DCollection(np.array([plane]), facecolors="red", edgecolors="black", linewidths=0.5, alpha = 0.6, zorder = 1000))
            pcs.append(pc)
        if self.obstacle_trajectory_boxes is not None:
            [[poly.remove() for poly in pc] for pc in self.obstacle_trajectory_boxes]
        self.obstacle_trajectory_boxes = [[self.ax_3d.add_collection(poly) for poly in pc] for pc in pcs]


        self.flags["obstacle_path"] = True
        pass

