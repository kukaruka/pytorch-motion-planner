# Import libraries
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure(figsize =(5, 5))
base_path = "C:/Users/user/Documents/pytorch-motion-planner/"

# data = np.load("../dumps/cross_1/imu_dump_1679227747.2054887.npy")
# data = np.load("../dumps/cross_1/imu_dump_1679227747.2054887.npy")
# data = np.load("../dumps/cross_1/imu_dump_1679233587.2736247.npy")
# data = np.load("../dumps/cross_1/imu_dump_1679235393.6009305.npy")
# data = np.load("../dumps/cross_1/imu_dump_1679235542.4687676.npy")

data = np.load(f"dumps/cross_1/imu_dump_1679235900.3652177.npy")
# data = np.vstack([data,np.load(f"dumps/cross_1/imu_dump_1679236160.2092087.npy")])
# data = np.vstack([data,np.load(f"dumps/cross_1/imu_dump_1679236290.2054958.npy")])
# data = np.vstack([data,np.load(f"dumps/cross_1/imu_dump_1679237624.3486154.npy")])
# data = np.vstack([data,np.load(f"dumps/cross_1/imu_dump_1679237421.7472696.npy")])

data = np.load(f"dumps/cross_1/imu_dump_1679237421.7472696.npy")



# data = np.load("C:\\Users\\user\\Documents\\pytorch-motion-planner\\dumps\\cross_1\\imu_dump_1679236290.2054958.npy")
# data = np.vstack([data,np.load("C:\\Users\\user\\Documents\\pytorch-motion-planner\\dumps\\hotel\\imu_dump_1679263068.8101075.npy")])

# Creating plot
bx = data[:,:3]
# plt.boxplot(bx) 
# show plot
# plt.show()

def exponential_smoothing(data, alpha):
    n = data.shape[0]
    exp_smooth_data = np.zeros_like(data)
    exp_smooth_data[0] = data[0]
    for i in range(1, n):
        exp_smooth_data[i] = exp_smooth_data[i - 1] + alpha*(data[i] - exp_smooth_data[i - 1])
    return exp_smooth_data

def running_mean(data, M):
    '''
    For corner cases it adjusts window size symmetrically for current index
    
    data - array, measurements
    M - parameter, window size
    '''
    n = data.shape[0]
    running_mean_data = np.zeros_like(data)
    lower_bound = int( M // 2 )
    upper_bound = int( (M - 1) // 2 )
    
    for i in range(lower_bound):
        window_size = 2*i + 1
        low_i = int( window_size // 2 )
        upp_i = int( (window_size - 1) // 2 )
        running_mean_data[i] = ( data[i-low_i:i+upp_i+1].sum() ) / window_size
    
    for i in range(lower_bound, n - upper_bound):
        running_mean_data[i] = ( data[i-lower_bound:i+upper_bound+1].sum() ) / M
    
    for i in range(n - upper_bound, n):
        j = n - i
        window_size = 2*j - 1
        low_i = int( window_size // 2 )
        upp_i = int( (window_size - 1) // 2 )
        running_mean_data[i] = ( data[i-low_i:i+upp_i+1].sum() ) / window_size
    
    return running_mean_data

from matplotlib.patches import Rectangle
figure, ax = plt.subplots(1, 4, figsize=(20, 3), sharey=True)

# x_sm = exponential_smoothing(data[:,0], 0.1)
print(np.array(data[:,0]).shape)
long_filtered = running_mean(data[:,0], 1000)
lat_filtered = running_mean(data[:,1], 1000)


# Blue zone
# ax[0].plot([0, data[0,3]], [1.5, 1.5], color='green', linewidth=0.5)
# ax[0].plot([0, data[0,3]], [-2., -2.], color='green', linewidth=0.5)
lightgreen = (216/255, 1., 219/255)
lightyellow = (1., 246/255, 183/255)
lightblue = (135/255, 221/255, 255/255)
lighred = (255/255, 198/255, 219/255)
gray = (200/255, 200/255, 200/255)
hatch = '////'
# Longtitudal
ax[0].add_patch(Rectangle((0., -2.), data[0,3], 3.5, fill=True, facecolor=lightblue, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[0].add_patch(Rectangle((0., 1.5), data[0,3], 2, fill=True, facecolor=lightyellow, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[0].add_patch(Rectangle((0., -5), data[0,3], 3, fill=True, facecolor=lightyellow, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[0].add_patch(Rectangle((0., -10), data[0,3], 5, fill=True, facecolor=lighred, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[0].add_patch(Rectangle((0., 3.5), data[0,3], 5, fill=True, facecolor=lighred, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))

# Lateral
ax[1].add_patch(Rectangle((0., -4.), data[0,3], 8, fill=True, facecolor=lightblue, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[1].add_patch(Rectangle((0., -5), data[0,3], 1, fill=True, facecolor=lightyellow, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[1].add_patch(Rectangle((0., 4), data[0,3], 1.5, fill=True, facecolor=lightyellow, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[1].add_patch(Rectangle((0., -10), data[0,3], 5, fill=True, facecolor=lighred, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[1].add_patch(Rectangle((0., 5.5), data[0,3], 5, fill=True, facecolor=lighred, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))

ax[0].plot(data[:,3]-data[0,3], long_filtered, 'b')
ax[0].set_xlabel('Time (s)')
ax[0].set_ylabel('Longtitudal acceleration (ms-2)')

ax[1].plot(data[:,3]-data[0,3],lat_filtered, 'b-')
ax[1].set_xlabel('Time (s)')
ax[1].set_ylabel('Lateral acceleration (ms-2)')

ax[0].set_xlim([0,40])
ax[0].set_ylim([-7,7])
ax[1].set_xlim([0,40])
ax[1].set_ylim([-11,7])

jerk_lon = running_mean((long_filtered[1:] - long_filtered[:-1])/(data[1:,3]-data[:-1,3]), 8000)

ax[3].add_patch(Rectangle((0., -2.), data[0,3], 3.5, fill=True, facecolor=lightblue, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[3].add_patch(Rectangle((0., 1.5), data[0,3], 2, fill=True, facecolor=lightyellow, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[3].add_patch(Rectangle((0., -5), data[0,3], 3, fill=True, facecolor=lightyellow, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[3].add_patch(Rectangle((0., -10), data[0,3], 5, fill=True, facecolor=lighred, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))
ax[3].add_patch(Rectangle((0., 3.5), data[0,3], 5, fill=True, facecolor=lighred, hatch=hatch, alpha=0.5, edgecolor=gray, linewidth=0.25))


import seaborn as sns
sns.set_style("whitegrid")
sns.violinplot(data=[long_filtered, long_filtered, long_filtered])
plt.show()